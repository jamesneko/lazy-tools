#!/usr/bin/env perl

use strict;
use warnings;
use utf8;
use v5.16;
use warnings FATAL => 'all';
binmode STDERR, ':utf8';
binmode STDOUT, ':utf8';

use FindBin qw/$RealBin/;
use lib "$RealBin/lib";

use Term::ANSIColor;
use Getopt::Long;
use File::Slurp;
use Try::Tiny;

# Use String::Diff - IF you have it installed.
my $HAVE_STRING_DIFF = eval {
	require String::Diff;
	String::Diff->import();
	1;
};

use LazyCat::CodeSearch qw/:all/;
use LazyCat::PrettyColours qw/note notepart notekv/;

my %opts;
GetOptions(\%opts, 'help!', 'dir=s', 'files-from=s', 'dry-run!');


sub help
{
	my ($err) = @_;
	say colored($err, 'bold red') if $err;
	note <<EOF;
Usage: $0 --dry-run 's/pattern/replacement/g;'
Tool for performing mass-subsitution on lots of files using standard Perl substitution (or any valid Perl) code. Defaults to current dir. You are strongly advised to use --dry-run until you are satisfied that the change will be what you want, and curating a list of files to affect beforehand and supplying with --files-from.";

Options:
	--dir <pathname> : Search for files from this directory instead.
	--files-from <textfile> : Get file list from here instead.
	--filenames-matching <regexp> : Filter only files matching this pattern.
	--dry-run : Do not apply changes.
EOF
	exit(1);
}


sub main
{
	help("Supply exactly one argument containing a Perl substitution, please!") unless @ARGV == 1;
	my $code = $ARGV[0];
	# Do a quick sanity check.
	apply_substitution_code($code, "");
	note colored("Substitution: \"$code\"", 'bold green');

	# We want to output to STDOUT for results, so turn on autoflush.
	$| = 1;
	
	my @files;
	if ($opts{'files-from'}) {
		notepart "Loading file list from '$opts{'files-from'}'... ", 'bold cyan';
		@files = split('\n', read_file($opts{'files-from'}));
	} else {
		notepart "Getting file list... ", 'bold cyan';
		@files = find_files($opts{dir} // ".");
	}
	if ($opts{'filenames-matching'}) {
		my $rx = qr/$opts{'filenames-matching'}/;
		notepart "Filtering...", 'bold cyan';
		@files = grep { $_ =~ /$rx/ } @files;
	}
	note scalar @files . " files.", 'bold cyan';
	
	grep_files(sub {
			my ($filepath, $text) = @_;
			try {
				apply_substitutions($filepath, $text, $code);
			} catch {
				notekv "Error caught processing $filepath :", $_, 'red';
				die $_;
			};
		}, {  }, @files);
	
}


# Do a line-by-line application of the given Perl transformation on the file we're currently working on.
# Returns the number of lines modified within this file.
sub apply_substitutions
{
	my ($filepath, $text, $code) = @_;
	my $file_lines_modified = 0;
	#### TODO hack can be applied here, use $text directly for multi-line substitutions.
	$text //= '';
	my @lines = split('\n', $text, -1);	# perldoc -f split, setting LIMIT to -1 allows trailing empty fields.

	# Go through each line and apply the code.
	for (my $i = 0; $i < @lines; ++$i) {
		my $linenum = $i + 1;
		my $line = apply_substitution_code($code, $lines[$i]);
		
		# Was the line changed?
		if ($line ne $lines[$i]) {
			$file_lines_modified++;
			# Standard 'grep' like behaviour, results are file name and line.
			if ($HAVE_STRING_DIFF) {
				display_modified_line_diff($filepath, $linenum, $lines[$i], $line);
			} else {
				display_modified_line_plain($filepath, $linenum, $lines[$i], $line);
			}
			# Now we've shown the diff, actually apply the change to this line.
			$lines[$i] = $line;
		}
	}

	# Did anything in the file change?
	if ($file_lines_modified) {
		if ($opts{'dry-run'}) {
			note "Not saving $filepath, --dry-run in effect.", 'yellow';
		} else {
			# Reconstruct and save the modified file.
			my $newtext = join("\n", @lines);
			note "Saving $filepath.", 'bold cyan';
			write_file($filepath, $newtext);
		}
	}
	return $file_lines_modified;
}


sub apply_substitution_code
{
	my ($code, $line) = @_;

	$_ = $line;
	my $rval = eval $code;
	$line = $_;
	die "Error in substitutions: $@" if $@;

	return $line;
}


sub display_modified_line_plain
{
	my ($filepath, $linenum, $oldline, $newline) = @_;
	print colored($filepath, 'cyan') . ":" . colored($linenum, 'cyan') . " ";
	print $newline;
	print "\n";
}


sub display_modified_line_diff
{
	my ($filepath, $linenum, $oldline, $newline) = @_;
	my $diff = String::Diff::diff_merge($oldline, $newline,
			remove_open  => color('bold on_red'),
			remove_close => color('reset'),
			append_open  => color('bold on_green'),
			append_close => color('reset'),
		);
	print colored($filepath, 'cyan') . ":" . colored($linenum, 'cyan') . " ";
	print $diff;
	print "\n";
}


help() if $opts{help};
help("--dir and --files-from are mutually exclusive.") if $opts{'dir'} && $opts{'files-from'};
main();
