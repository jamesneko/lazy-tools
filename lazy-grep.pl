#!/usr/bin/env -S perl -CSDA

use strict;
use warnings;
use utf8;
use v5.16;
use warnings FATAL => 'all';
binmode STDERR, ':utf8';
binmode STDOUT, ':utf8';

use FindBin qw/$RealBin/;
use lib "$RealBin/lib";

use Term::ANSIColor;
use Getopt::Long;
use Try::Tiny;

use LazyCat::CodeSearch qw/:all/;
use LazyCat::PrettyColours qw/note notepart notekv/;

my %opts;
GetOptions(\%opts, 'help!', 'dir=s', 'files-from=s', 'filenames-matching=s', 'files-only!', 'unique!', 'insensitive!');

my @colours = map { s/_/ /gr } qw/bold_red bold_cyan bold_green bold_yellow bold_blue bold_magenta/;


sub help
{
	my ($err) = @_;
	note $err, 'bold red' if $err;
	note <<EOF, 'reset';
Usage: $0 <pcre> [ <pcre> ... ]
Tool for grepping source code for PCRE expressions. Defaults to current dir. All other args are expected to be regexp strings to be put in a m//.";

Options:
	--dir <pathname> : Search for files from this directory instead.
	--files-from <textfile> : Get file list from here instead.
	--files-only : Just list each filename that matches.
	--unique : Collate the unique matching parts of the expressions and provide a summary.
	--insensitive : Case insensitive (Prepends (?i) to your regexps)
	--filenames-matching <regexp> : Filter only files matching this pattern.
EOF
	exit(1);
}


sub main
{
	my @regexps = @ARGV;
	@regexps = map { "(?i)$_" } @regexps if $opts{insensitive};
	@regexps = map { qr/$_/ } @regexps;
	die "Supply at least one PCRE please!" unless @regexps;

	# We want to output to STDOUT for results, so turn on autoflush.
	$| = 1;

	my @files;
	if ($opts{'files-from'}) {
		notepart "Loading file list from '$opts{'files-from'}'... ", 'bold cyan';
		@files = split('\n', read_file($opts{'files-from'}));
	} else {
		notepart "Getting file list... ", 'bold cyan';
		@files = find_files($opts{dir} // ".");
	}
	if ($opts{'filenames-matching'}) {
		my $rx = qr/$opts{'filenames-matching'}/;
		notepart "Filtering...", 'bold cyan';
		@files = grep { $_ =~ /$rx/ } @files;
	}
	note scalar @files . " files.", 'bold cyan';

	my %unique_matches;

	grep_files(sub {
			my ($filepath, $text) = @_;
			# Go check each line individually.
			try {
				grep_file_lines($filepath, $text, \@regexps, \%unique_matches);
			} catch {
				notekv "Error caught processing $filepath :", $_, 'red';
				die $_;
			};
		}, {}, @files);

	# Summarise what we learned with --unique.
	if ($opts{unique}) {
		note((scalar keys %unique_matches) . " Unique matches:-", 'bold cyan');
		foreach my $match (sort keys %unique_matches) {
			say colored($match, $colours[0]) . ": " . $unique_matches{$match};
		}
	}
}


# Do a line-by-line grep on the file we're currently working on.
# Returns the number of matches within this file.
sub grep_file_lines
{
	my ($filepath, $text, $regexps, $unique_matches) = @_;
	my $file_matches = 0;
	my @lines = split('\n', $text||'');

	# Go through each line and apply each regexp.
	for (my $i = 0; $i < @lines; ++$i) {
		my $line = $lines[$i];
		my $linenum = $i + 1;
		my $line_matches = 0;

		for (my $r = 0; $r < @$regexps; ++$r) {
			my $regexp = $regexps->[$r];
			my $colour = $colours[$r % @colours];
			# Quick and dirty way to highlight matching sections.
			if ($line =~ m/$regexp/g) {	#### TODO: Can't use 'while' yet because we're modifying $line and changing the rx match pos.
				my ($FRONT, $MATCH, $BACK) = ($`, $&, $');
				$line_matches++;
				$file_matches++;
				$unique_matches->{$MATCH}++ if $opts{unique};

				# Colour the line using ANSI escapes.
				$line = $FRONT . colored($MATCH, $colour) . $BACK;
			}
		}

		# Did the line match?
		if ($line_matches) {
			if ($opts{'files-only'} || $opts{'unique'}) {
				# No individual match text.

			} elsif (length $line > 1000) {
				print colored($filepath, 'cyan') . ":" . colored($linenum, 'cyan') . " ";
				print colored("(huge line matches)\n", 'bold white');

			} else {
				# Standard 'grep' like behaviour, results are file name and line.
				print colored($filepath, 'cyan') . ":" . colored($linenum, 'cyan') . " ";
				print $line;
				print "\n";
			}
		}
	}

	# Did anything in the file match?
	if ($file_matches) {
		if ($opts{'files-only'}) {
			# Results are raw filenames, shown as we find them.
			say $filepath;
		} elsif ($opts{'unique'}) {
			# Digesting unique matches, would be nice to see some progress while we do so.
			note "$filepath: $file_matches matches.", 'cyan';
		} else {
			# Standard 'grep' like behaviour, we've already shown per-line progress.
		}
	}
	return $file_matches;
}


sub quote_regexp
{
	my ($re) = @_;
	return qr/$re/;
}


help() if $opts{help};
help("--dir and --files-from are mutually exclusive.") if $opts{'dir'} && $opts{'files-from'};
help("--files-only and --unique are mutually exclusive.") if $opts{'files-only'} && $opts{'unique'};
main();
