#!/usr/bin/env -S perl -CSDA
use Mojo::Base -strict, -signatures;
binmode STDERR, ':utf8';
binmode STDOUT, ':utf8';

use FindBin qw/$RealBin/;
use lib "$RealBin/lib";

use Term::ANSIColor;
use Getopt::Long;

use Try::Tiny;
use DateTime;
use DateTime::Duration;
use JSON; # just for debugging atm

use LazyCat::PrettyColours qw/note notepart notekv/;

my %OPTS;
GetOptions(\%OPTS, 'help!');

my @colours = map { s/_/ /gr } qw/bold_red bold_cyan bold_green bold_yellow bold_blue bold_magenta/;


sub help($err = '') {
  note $err, 'bold red' if $err;
  note <<EOF, 'reset';
Usage: $0 'today + 10 days'
Tool to do date maths.

EOF
  exit 1;
}


my $RX_POSITION = qr/
    \b(?<position>
      now
    | today
    | tomorrow
    | yesterday
    | (?<isodate> (?<year>\d{4})-?(?<month>\d{2})-?(?<day>\d{2}) )
    )\b
  /xi;

my $RX_INTERVAL = qr/
    \b(?<interval>
      (?<days>   \d+) \s+ days?
    | (?<weeks>  \d+) \s+ weeks?
    | (?<months> \d+) \s+ months?
    | (?<years>  \d+) \s+ years?
    )\b
  /xi;

sub parse_datecalc($what) {
  if ($what =~ /^
     (?<from> $RX_POSITION \s+)?
     (?<op> \+ | -)
     \s*
     $RX_INTERVAL
     \s*
     (,\s*(?<more> .*))?
    $/xi) {
    my $obj = {%+};
    $obj->{then} = parse_datecalc($+{more}) if $+{more};
    return $obj;
  }
  return { error => "Couldn't parse a date calculation in '$what'." };
}


sub today() {
  return DateTime->now()->truncate(to => 'day');
}


sub eval_datecalc($calc) {
  my @results;
  my $end;
  try {
    while (1) {
      my $pos;
      $pos = $end if $end;  # continue from previous result if using comma operator
      if ($calc->{position}) {
        $pos = today() if $calc->{position} =~ /^(now|today)$/;
        $pos = today()->subtract(days => 1) if $calc->{position} eq 'yesterday';
        $pos = today()->add(days => 1) if $calc->{position} eq 'tomorrow';
      }
      if ($calc->{isodate}) {
        $pos = DateTime->new(
          year  => $calc->{year},
          month => $calc->{month},
          day   => $calc->{day},
        );
      }
      
      # Construct DateTime compatible interval
      my %int;
      foreach (qw/days weeks months years/) {
        $int{$_} = $calc->{$_} if $calc->{$_};
      }
      
      # Do the operation
      $end = $pos->add(%int) if $calc->{op} eq '+';
      $end = $pos->subtract(%int) if $calc->{op} eq '-';
      push @results, $end->clone();

      # Continue?
      last unless $calc->{then};
      $calc = $calc->{then};
    }
  } catch {
    note "Something failed: $_", 'bold red';
    exit 3;
  };
  return @results;
}


sub main($verb, @args) {
  # ... handle known verbs for calendar maintenance etc.?
  # ...
  
  # catchall, one argument, we are doing date maths.
  if (@args == 0) {
    my $calc = parse_datecalc($verb);
    if ($calc->{error}) {
      note $calc->{error}, 'bold red';
      exit 2;
    }
    # say "Debug: " . JSON->new->pretty->encode($calc);
    my @answers = eval_datecalc($calc);
    foreach my $answer (@answers) {
      say $answer->ymd();
    }

  } else {
    help('If you are doing date maths, just supply one argument please.');
  }
}


help() if $OPTS{help};
help() if @ARGV == 0;
main(@ARGV);
