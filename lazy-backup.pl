#!/usr/bin/env perl
# Bah, env (GNU coreutils) 8.26 does not support -S yet, so -CSDA is still problematic. 9.1 does.
# Happily, we can set utf8 on stdout/stdin ourselves, and can read filename patterns from a JSON config file.

use strict;
use warnings;
use utf8;
use v5.16;
use warnings FATAL => 'all';
binmode STDERR, ':utf8';
binmode STDOUT, ':utf8';

use FindBin qw/$RealBin/;
use lib "$RealBin/lib";

use File::Temp;
use File::Glob ':bsd_glob';
use File::Basename;
use Term::ANSIColor;
use Getopt::Long;
use List::Util qw/sum0/;

use JSON;
use File::Slurp;

use LazyCat::PrettyColours qw/note notepart notekv/;

#### NOTES
# ideally we'd make a specific config file that needs to be referenced to do the backup parameters,
# so it's identical settings each time.
# but for now we need a quick one off.

# I think the A in -CSDA will cause issues for this one if we attempt unicode things.
# Likewise if we go JSON config file.

my %OPTS = (
  upload => 1,  # to support --no-upload
  cleanup => 1,  # --no-cleanup
  'base-config' => "$ENV{HOME}/.lazy-backup-base.config",
  'drop-config' => 0,
  'drop-base-config' => 0,
);
GetOptions(\%OPTS, 'help!', 'inspect=s', 'extract=s', 'upload!', 'cleanup!', 'move-to=s', 'dry-run!', 'base-config=s', 'drop-config!', 'drop-base-config!', 'print-config!');


my $DEFAULT_CONFIG_NAME = ".lazy-backup.config";
my %DEFAULT_BASE_CONFIG = (
  workdir => "/tmp/lazy-backup",
  s3cfg => "$ENV{HOME}/.s3cfg",
  s3dir => "s3://lazy-backup/backups",
);


sub help
{
	my ($err) = @_;
	note $err, 'bold red' if $err;
	note <<EOF, 'reset';
Usage: $0 <path/to/$DEFAULT_CONFIG_NAME>
       $0 (--drop-config|--drop-base-config)
       $0 --inspect <archive file>
       $0 --extract <archive file>

Options:
  --no-upload : Don't do the s3 step.
  --no-cleanup : Leave files in working dir.
  --move-to <dir> : Move local archives somewhere (keep them).
  --dry-run : Don't run anything, just say what would be run.
  --base-config <lazy-backup-base.config> : Change where we source defaults from.
  
  --drop-config : Write a sample lazy-backup.config file in the current dir.
  --drop-base-config : Write a sample $OPTS{'base-config'}.
  --print-config : For debugging.
EOF
	exit(1);
}


sub shellquote
{
  return map {
    my $escaped = $_ =~ s/'/'"'"'/gr;
    "'$escaped'";
  } @_;
}


sub load_base_config
{
  my $base = { %DEFAULT_BASE_CONFIG };
  if (-f $OPTS{'base-config'}) {
    my $data = read_file($OPTS{'base-config'}, binmode => ':raw', err_mode => 'croak');
    $base = JSON->new->relaxed->decode($data);
  }
  return $base;
}

sub load_config
{
  my ($filename) = @_;
  die "Config file '$filename' does not exist!" unless -f $filename;

  my $base = load_base_config();

  my $data = read_file($filename, binmode => ':raw', err_mode => 'croak');
  my $json = JSON->new->relaxed->decode($data);
  return { %$base, %$json };
}


sub check_config
{
  my ($config) = @_;
  foreach my $key (qw/include basename password workdir s3cfg s3dir/) {
    die "Mandatory key '$key' not found in config file." unless exists $config->{$key};
  }
  die "No password specified in config file!" unless $config->{password};
  die "No directories included to backup!" unless @{$config->{include}} > 0;
}


sub do_drop_config
{
  my ($outfilename) = @_;
  help("Refusing to write $outfilename : File already exists.") if -f $outfilename;
  my $text = <<EOF;
{
  "include": ["."],              # The directory/directories to back up.
  "exclude": [".cache", "local"],    # tar(3) --exclude filename globs.
  "chdir": "",                       # Directory to chdir to before tar; default is whereever this config file is. Relative to this file.
  "basename": "test-backup.{DATE}",  # What to call the archive. Extension will be added automatically.

  # Defaults, generally set in $OPTS{'base-config'} but can be overridden here:
  # "workdir": "$DEFAULT_BASE_CONFIG{workdir}",  # Where to assemble the archive.
  # "s3cfg": "$DEFAULT_BASE_CONFIG{s3cfg}",      # Where the s3cmd config is.
  # "s3dir": "$DEFAULT_BASE_CONFIG{s3dir}",      # Where to upload the archive to.
  # "password": "",                              # 7z archive password.
}
EOF
  if ($OPTS{'dry-run'}) {
    notekv "Dry run: Would write file", $text, 'yellow';
  } else {
    write_file($outfilename, { binmode => 'utf8' }, $text);
    notekv "Wrote file", $outfilename;
  }
  exit 0;
}


sub do_drop_base_config
{
  my ($outfilename) = @_;
  help("Refusing to write $outfilename : File already exists.") if -f $outfilename;
  my $text = <<EOF;
{
  "workdir": "$DEFAULT_BASE_CONFIG{workdir}",  # Where to assemble the archive.
  "s3cfg": "$DEFAULT_BASE_CONFIG{s3cfg}",      # Where the s3cmd config is.
  "s3dir": "$DEFAULT_BASE_CONFIG{s3dir}",      # Where to upload the archive to.
  "password": "",                              # 7z archive password.
}
EOF
  if ($OPTS{'dry-run'}) {
    notekv "Dry run: Would write file", $text, 'yellow';
  } else {
    write_file($outfilename, { binmode => 'utf8' }, $text);
    notekv "Wrote file", $outfilename;
  }
  exit 0;
}


sub do_print_config
{
  my ($config_filename) = @_;
  my $config = load_config($config_filename);
  say JSON->new->pretty->encode($config);
  exit 0;
}


sub do_inspect_archive
{
  my ($filename) = @_;
  my $base_config = load_base_config();

  my @passworded = shellquote "-p$base_config->{password}";
  run(join(' ',
    '7z', 'x', '-so', @passworded, $filename,
    ' | ', 'tar', '-tv', '-f', '-'));
  exit 0;
}


sub do_extract_archive
{
  my ($filename) = @_;
  my $base_config = load_base_config();

  my @passworded = shellquote "-p$base_config->{password}";
  run(join(' ',
    '7z', 'x', '-so', @passworded, $filename,
    ' | ', 'tar', '-xv', '-f', '-'));
  exit 0;
}


sub run
{
  my @cmd = @_;
  if ($OPTS{'dry-run'}) {
    notekv "Dry run: NOT Running", (map { "$_" } join(' ', @cmd)), 'yellow';

  } else {
    notekv "Running", join(' ', @cmd), 'cyan';
    system(@cmd);
    my $ecode = $? >> 8;
    die "Command '$cmd[0]' exited with code $ecode" if $ecode;
  }
}


sub exclude_dirs
{
  my ($config) = @_;
  return () unless $config->{exclude};
  return map { ('--exclude', $_) } @{$config->{exclude}};
}


sub test_cmd
{
  my ($cmd) = @_;
  my $output = `$cmd`;
  my $ecode = $? >> 8;
  return $ecode == 0;
}


sub precondition_checks
{
  my ($config) = @_;
  help "No .s3cfg file found!" unless -f $config->{s3cfg};
  help "No s3cmd available!" unless test_cmd "s3cmd --help";
  help "No 7z available!" unless test_cmd "7z --help";
}


sub make_workdir
{
  my ($workdir) = @_;
  if ( ! -d $workdir) {
    system("mkdir", "-p", $workdir);
    die "Couldn't make '$workdir'!" unless -d $workdir;
  }
  my $tempdir = File::Temp->newdir(TEMPLATE => "$workdir/lb-XXXXXXXX", CLEANUP => $OPTS{cleanup});
  return $tempdir;
}


sub apply_filename_substitutions
{
  my ($template) = @_;
  my %vars = ();
  my ($y, $m, $d) = (localtime)[5,4,3];
  $vars{DATE} = sprintf("%04d%02d%02d", 1900 + $y, 1 + $m, $d);

  my $filename = $template;
  foreach my $varname (keys %vars) {
    $filename =~ s/\{$varname\}/$vars{$varname}/g;
  }
  return $filename;
}


sub format_byte_size
{
  my ($bytes) = @_;
  my $unit = 'B';
  if ($bytes > 1024 * 1024 * 1024) {
    $bytes /= 1024 * 1024 * 1024;
    $unit = 'GiB';
  } elsif ($bytes > 1024 * 1024) {
    $bytes /= 1024 * 1024;
    $unit = 'MiB';
  } elsif ($bytes > 1024) {
    $bytes /= 1024;
    $unit = 'KiB';
  }
  return sprintf("%.01f %s", $bytes, $unit);
}


sub list_s3_files
{
  my ($config, $s3path) = @_;
  my $s3cfg = $config->{s3cfg};
  my $text = `s3cmd --config="$s3cfg" ls $s3path`;
  my @lines = split("\n", $text);
  my $files = {};
  foreach my $line (@lines) {
    next unless $line =~ /^(?<date>\S+)\s+(?<time>\S+)\s+(?<size>\d+)\s+(?<name>.*)$/;
    $files->{$+{name}} = $+{size};
  }
  return $files;
}


sub verify_files
{
  my ($config, $upload_dir, $uploaded_files, @archives) = @_;
  my $errs = 0;
  foreach my $archive_filename (@archives) {
    my $basename = basename $archive_filename;
    my $remote_name = "${upload_dir}${basename}";
    unless ($uploaded_files->{$remote_name}) {
      notekv $basename, "Was not uploaded to S3", 'red';
      $errs++;
      next;
    }
    unless (-s $archive_filename == $uploaded_files->{$remote_name}) {
      notekv $basename, "Incomplete upload", 'red';
      $errs++;
      next;
    }
    notekv $basename, format_byte_size($uploaded_files->{$remote_name}) . " OK", 'green';
  }
  return $errs;
}


sub upload_to_s3
{
  my ($config, $filebasename, @archives) = @_;
  die "No archive files were created, strange" unless @archives && ! $OPTS{'dry-run'};

  my $upload_dir = $config->{s3dir};
  $upload_dir = "$upload_dir/" unless $upload_dir =~ m{/$};

  run('s3cmd', "--config=" . $config->{s3cfg}, 'put', @archives, $upload_dir) if $OPTS{upload};

  if ($OPTS{'dry-run'}) {
    note "Dry run: can't list files in s3.", 'yellow';
    return 0;

  } else {
    my $uploaded_files = list_s3_files($config, "${upload_dir}${filebasename}.*");
    my $errs = verify_files($config, $upload_dir, $uploaded_files, @archives);
    if ($errs) {
      note "Uploaded but with $errs errors!", 'bright_red';
    } else {
      note "Uploaded OK!", 'bright_green';
    }
    return $errs;
  }
}


sub main
{
  my ($config_filename) = @_;
  my $config = load_config($config_filename);
  check_config($config);
  precondition_checks($config);
  notekv "Config dir", dirname $config_filename;

  # Everything from here needs to be relative to the .lazy-backup.config file
  chdir(dirname $config_filename) or die "Couldn't chdir to config directory!";

  my $tempdir = make_workdir($config->{workdir});
  notekv "Working dir", $tempdir;

  my $filebasename = apply_filename_substitutions($config->{basename});
  
  # ---- ARCHIVE ----
  my @chdir = ("-C", shellquote $config->{chdir}) if $config->{chdir};
  my @include = shellquote @{$config->{include}};
  my @exclude = shellquote exclude_dirs($config);
  my @passworded = shellquote "-p$config->{password}";
  my @outfile = shellquote "$tempdir/$filebasename.tar.7z";
  run(join(' ',
    'tar', @chdir, @exclude, '-cv', '-f', '-', @include,
    ' | ', '7z', 'a', '-si', '-v4g', '-mhe=on', @passworded, @outfile));

  my @archives = bsd_glob("$tempdir/$filebasename.*");
  if (@archives) {
    my $size = sum0 map { -s $_ } @archives;
    notekv "Archive made", (scalar @archives) . " part" . (@archives > 1 ? 's' : '') . ", totalling " . format_byte_size($size), 'green';
  }

  # ---- UPLOAD ----
  if ($OPTS{upload}) {
    my $errs = upload_to_s3($config, $filebasename, @archives);
    exit 1 if $errs;
  }

  # ---- MOVE (keep the local file somewhere) ----
  if ($OPTS{'move-to'}) {
    die "Destination $OPTS{'move-to'} does not exist!" unless -d $OPTS{'move-to'};
    run('mv', '-v', @archives, $OPTS{'move-to'});
  }
}


help() if $OPTS{help};
do_drop_base_config($OPTS{'base-config'}) if $OPTS{'drop-base-config'};
do_drop_config($DEFAULT_CONFIG_NAME) if $OPTS{'drop-config'};

do_inspect_archive($OPTS{inspect}) if $OPTS{inspect};
do_extract_archive($OPTS{extract}) if $OPTS{extract};

help("Need to supply a $DEFAULT_CONFIG_NAME as an argument!") unless $ARGV[0];
do_print_config($ARGV[0]) if $OPTS{'print-config'};
main($ARGV[0]);
