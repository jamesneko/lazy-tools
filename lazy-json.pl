#!/usr/bin/env -S perl -CSD
use Mojo::Base -strict, -signatures;
binmode STDERR, ':utf8';
binmode STDOUT, ':utf8';

use FindBin qw/$RealBin/;
use lib "$RealBin/lib";

use Term::ANSIColor;
use Getopt::Long;

use Try::Tiny;
use JSON;
use File::Slurp;

use LazyCat::PrettyColours qw/note notepart notekv/;

my %OPTS;
GetOptions(\%OPTS, 'help!');


sub help($err = '') {
  note $err, 'bold red' if $err;
  note <<EOF, 'reset';
Usage: $0 summarise filename.json
Make sense of huge JSON blobs.

EOF
  exit 1;
}


sub load_json($filename) {
  my $data = read_file($filename, binmode => ':raw', err_mode => 'croak');
  return JSON->new->relaxed->decode($data);
}


sub output($stack, $str, $colour = 'cyan') {
  return { indent => $stack->{indent}, summary => $str, colour => $colour };
}


sub summarise_array($stack, $name, $arr) {
  return output($stack, "$name: An array with " . (length $arr) . " items.", 'bold yellow');
}


sub summarise_object($stack, $name, $obj) {
  my @keys = sort keys %$obj;
  my $out = output($stack, "$name: An object with keys " . join(", ", @keys) . " :", 'bold green');
  my @otherout = map { summarise_value($stack, $_, $obj->{$_}) } @keys;
  return ($out, @otherout);
}


sub summarise_scalar($stack, $name, $val) {
  return output($stack, "$name: scalar value of $val.");
}


sub summarise_value($stack, $name, $val) {
  $stack = { %$stack };
  $stack->{indent}++;
  return summarise_array($stack, $name, $val)  if ref $val eq 'ARRAY';
  return summarise_object($stack, $name, $val) if ref $val eq 'HASH';
  return summarise_scalar($stack, $name, $val);
}


sub do_summarise($filename) {
  my $root = load_json($filename);
  my $stack = { indent => 0 };
  my @lines = summarise_value($stack, "The root", $root);
  foreach my $line (@lines) {
    note(("  " x $line->{indent}) . $line->{summary}, $line->{colour});
  }
}


sub main($verb, @args) {
  if ($verb =~ /summari[sz]e/) {
    do_summarise($_) foreach @args;
  } else {
    help("Unknown verb '$verb'.");
  }
}


help() if $OPTS{help};
help() if @ARGV == 0;
main(@ARGV);
