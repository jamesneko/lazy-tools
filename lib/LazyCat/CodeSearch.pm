package LazyCat::CodeSearch;

use warnings;
use strict;
use v5.16;
use Exporter 'import';
use File::Find;

use Try::Tiny;
use File::Slurp;
use Term::ProgressBar;

our @EXPORT_OK = qw/find_files grep_files/;
our %EXPORT_TAGS = ( all => [@EXPORT_OK] );

package File::Slurp {
  use warnings FATAL => 'all';
};

#---------------------------------------------------------------------------------------------------
# Function : Call File::Find to get a big list of file path names, relative to the current dir.
# Args     : Directory name.
#            Optional key/value flags:
#              None yet.
# Returns  : List of matching file paths.
#---------------------------------------------------------------------------------------------------
sub find_files
{
  my ($dir, %flags) = @_;
  my @files;
  # Pass File::Find an anonymous sub that closes over @files so we can build up a list of all the files
  # we need to process.
  find({
      preprocess => sub {
        return
            grep { $_ !~ m{^\.(DS_Store|_.*)$} }           # Apple junk.
            grep { $_ !~ m{^\.(git|hg|svn|cvs)$} }         # VCS metadata dirs
            grep { $_ !~ m{^node_modules$} }               # Nooo.
            grep { $_ !~ m{^.gradle$} }                    # Likewise.
            grep { $_ !~ m{\.(class|war|jar|pyc)$} }       # Compiled code.
            grep { $_ !~ m{^(\.next|coverage)$} }          # Usually irrelevant. Would like to make this config.
            grep { $_ !~ m{\.(png|jpg)$} }                 # Images?
            grep { $_ !~ m{\.(gz|tar|zip)$} }              # Archives?
            grep { $_ !~ m{\.(bin)$} }                     # Most likely binary.
            @_;
      },
      wanted => sub {
        my $fullpathname = $File::Find::name;	# NOT a utf8 string! FS-dependendent binary string that _might_ be encoded with UTF-8.
        
        return unless -f $fullpathname;                                # Only process actual files. (must use name and not _ here, stat on that doesn't seem to do what you want)
        return if $fullpathname =~ m{/\._};                           # Bloody MacOS resource forks!
        return if $fullpathname =~ m{/(archived?|backup)[^/]*/}i;     # Backup dirs.
        
        push @files, $fullpathname;
      },
      follow_skip => 2,  # If we get duplicate dirs, files etc. during find, ignore and carry on.
      no_chdir => 1,     # We want our current dir consistent after Find does its work.
    }, $dir );
  return @files;
}


#---------------------------------------------------------------------------------------------------
# Function : Given a list of file path names from find_files(), load each of them in turn and execute a callback
#            on the text of the file. Anything the callback returns is appended to a list which is returned
#            by grep_files.
#
#            The callback will be passed three arguments:
#              - The filepath being processed
#              - The file's contents as text (possibly undefined)
#              - Any errors that occurred reading the file.
# Args     : - code reference - a callback to be run on each file.
#            - hashref of options, currently only one option is supported:
#              { progress_bar => 1 } - uses Term::ProgressBar to display a progress bar in the terminal.
#            - list of file names.
# Returns  : List of returned values from the callback.
#---------------------------------------------------------------------------------------------------
sub grep_files
{
  my ($callback, $options, @files) = @_;
  my @return_values;
  my $progress;
  if ($options->{progress_bar}) {
    $progress = Term::ProgressBar->new({count => scalar @files, ETA => 'linear'});
  }
  
  my $done = 0;
  my $next_progress = 0;
  foreach my $filepath (@files) {
    my ($text, $err);
    try {
      $text = read_file($filepath, binmode => ':raw', err_mode => 'croak');
    } catch {
      $err = $_;
    };
    my @r = &$callback($filepath, $text, $err);
    push @return_values, @r;
    $done++;
    if ($progress && $done >= $next_progress) {
      $next_progress = $progress->update($done);
    }
  }
  return @return_values;
}


1;
