requires "Try::Tiny";
requires "File::Slurp";
requires "List::MoreUtils", '>=', '0.420_001';
requires "Term::ProgressBar";
requires "Mojo::Base";  # mostly for 'use Mojo::Base -signatures;'
requires "DateTime";
requires "JSON";
